using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTWO
{
    public class GameWon : MonoBehaviour
    {
        /// <summary>
        /// Determinse the Y position to tween to
        /// </summary>
        [SerializeField]
        private float endValue;

        /// <summary>
        /// Determines the duration of the tween
        /// </summary>
        [SerializeField]
        private float moveDuration;


        /// <summary>
        /// Called once on Startup
        /// </summary>
        private void Start()
        {
            // Tweening the GameObject with this script to the center of the UI, on completion we call the LoadMainMenu Method
            transform.DOLocalMoveY(endValue, moveDuration)
                     .OnComplete(LoadMainMenu);
        }

        /// <summary>
        /// Laods the MainMenu
        /// </summary>
        void LoadMainMenu()
        {
            // Unity Method to load scenes, should load MainMenu-Scene with a string
            SceneManager.LoadScene("MainMenu");
        }
    }
}