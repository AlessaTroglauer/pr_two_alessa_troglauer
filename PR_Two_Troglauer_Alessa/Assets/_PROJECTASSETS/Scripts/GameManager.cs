using UnityEngine;
using UnityEngine.UI;

namespace PRTWO
{
    public class GameManager : MonoBehaviour
    {
        /// <summary>
        /// Reference to the Boss Component
        /// </summary>
        [SerializeField]
        private Boss boss;

        /// <summary>
        /// Reference to the Player Component
        /// </summary>
        [SerializeField]
        private Player player;

        /// <summary>
        /// Reference to the Boss Healthbar
        /// </summary>
        [SerializeField]
        private Slider bossHealthbarSlider;

        /// <summary>
        /// Checks if the game is won or not
        /// </summary>
        [SerializeField]
        private bool gameWon;

        /// <summary>
        /// Reference to the GameWon GameObject
        /// </summary>
        [SerializeField]
        private GameObject gameWonScreen;


        /// <summary>
        /// Called every frame
        ///
        /// Setting the healthbar value to the actual boss health
        /// If the health is zero, so is the slider value, also setting the Playerstate to IDLE
        ///
        /// Enabling the gameWonScreen GameObject when the boss is dead
        /// </summary>
        private void Update()
        {
            bossHealthbarSlider.value = boss.Health;

            if (boss.Health <= 0)
            {
                player.State              = PlayerState.IDLE;
                bossHealthbarSlider.value = 0f;
                boss.Health               = 0;
            }

            if (!gameWon && !boss.BossAlive)
            {
                FinishGame();
            }
        }

        /// <summary>
        /// Setting up that the game is won and enabling the gameWonScreen GameObject
        /// </summary>
        private void FinishGame()
        {
            gameWon = false;
            gameWonScreen.SetActive(true);
        }
    }
}