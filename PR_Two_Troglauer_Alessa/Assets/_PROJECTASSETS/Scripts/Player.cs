﻿using UnityEngine;

namespace PRTWO
{
    public class Player : MonoBehaviour
    {
        [SerializeField]
        private string playerName;

        [SerializeField]
        public Boss boss;

        private PlayerState playerState;

        public string PlayerName
        {
            get
            {
                return playerName;
            }
            set
            {
                playerName = value;
            }
        }

        public PlayerState State
        {
            get
            {
                return playerState;
            }
            set
            {
                playerState = value;
            }
        }

        /// <summary>
        /// Called once on startup
        ///
        /// Setting the playerstate to ATTACK
        /// </summary>
        private void Start()
        {
            State = PlayerState.ATTACK;
        }


        /// <summary>
        /// Attacks the Enemy, has int parameter
        ///
        /// Checking first if the player is in playerstate ATTACK, if so we print a log that the player
        /// is attacking the boss, also printing the playername
        ///
        /// Checking if the health is below zero, if so we return
        ///
        /// Also checking if the health is above 0, if so we call the Damage method of the boss with the parameter
        /// </summary>
        /// <param name="enemy"></param>
        /// <param name="attackPower"></param>
        public void AttackEnemy(int attackPower)
        {
            if (State <= PlayerState.ATTACK)
            {
                Debug.LogFormat("{0} is attacking Boss!", PlayerName);

                if (boss.Health <= 0)
                {
                    return;
                }

                if (boss.Health > 0)
                {
                    boss.Damage(attackPower);
                }
            }
        }
    }
}